import AWS from "aws-sdk"
import {Context} from "aws-lambda"
import {ListObjectsRequest, ListObjectsOutput} from "aws-sdk/clients/s3";
import {AWSError} from "aws-sdk/lib/error";
import * as _ from "lodash"
import {Data, PutRecordInput, PutRecordOutput} from "aws-sdk/clients/kinesis";


exports.handler = async (event:any,context: Context) => {
    let s3 = new AWS.S3();
    let kinesis = new AWS.Kinesis();
    let s3BucketObjectsRequest: ListObjectsRequest = {
        Bucket: "econ-dev-network-articles"
    };
    s3.listObjects(s3BucketObjectsRequest, (err: AWSError, data: ListObjectsOutput) => {
        if(!_.isNull(err)){
            console.error(err);
        }else{
            console.log("Data Key Success - Starting Kinesis Push");
            const contents = data.Contents;
            if(contents){
                contents.forEach(content => {
                    let kinesisData: Data = JSON.stringify(content);
                    let partitionKey: string = (!_.isUndefined(content.ETag))? content.ETag : "default";
                    let kinesisRecord: PutRecordInput = {
                        StreamName: "econ-network-dev-s3-incoming",
                        Data: kinesisData,
                        PartitionKey: partitionKey
                    };
                    kinesis.putRecord(kinesisRecord, (err: AWSError, data: PutRecordOutput) =>{
                        if(!_.isNull(err)){
                            console.log(err);
                        }else {
                            console.log(data);
                        }
                    })
                });
            }
        }
    })
};