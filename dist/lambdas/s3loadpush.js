"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const _ = __importStar(require("lodash"));
exports.handler = (event, context) => __awaiter(this, void 0, void 0, function* () {
    let s3 = new aws_sdk_1.default.S3();
    let kinesis = new aws_sdk_1.default.Kinesis();
    let s3BucketObjectsRequest = {
        Bucket: "econ-dev-network-articles"
    };
    s3.listObjects(s3BucketObjectsRequest, (err, data) => {
        if (!_.isNull(err)) {
            console.error(err);
        }
        else {
            console.log("Data Key Success - Starting Kinesis Push");
            const contents = data.Contents;
            if (contents) {
                contents.forEach(content => {
                    let kinesisData = JSON.stringify(content);
                    let partitionKey = (!_.isUndefined(content.ETag)) ? content.ETag : "default";
                    let kinesisRecord = {
                        StreamName: "econ-network-dev-s3-incoming",
                        Data: kinesisData,
                        PartitionKey: partitionKey
                    };
                    kinesis.putRecord(kinesisRecord, (err, data) => {
                        if (!_.isNull(err)) {
                            console.log(err);
                        }
                        else {
                            console.log(data);
                        }
                    });
                });
            }
        }
    });
});
